package com.wwzd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class EntitiesRelationsVisualizerApplication {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
            }
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(EntitiesRelationsVisualizerApplication.class, args);
    }

    //todo
    // + UpdateBuilder
    // + Move Clarin Status Checking to seperate class
    // + Make analyze receving ZIP as bytes and send it to get ID
    // + Calculate strength of entities relations
    // + Create new endpoint to return relations calculations results
    // - Refactor Project Controller
    // - remove redundant wordsNumber field from Project class
    // - refactor FilesManager
    // + update banner with new project name

}
