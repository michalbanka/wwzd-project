package com.wwzd.service;

import com.wwzd.model.db.Project;
import com.wwzd.model.db.Section;
import com.wwzd.model.db.Word;
import com.wwzd.service.db.ProjectDao;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Log4j2
@Service
public class ProjectService {

    private final ProjectDao projectDao;

    @Autowired
    public ProjectService(ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    public List<Section> executePerEachWord(Project project, Consumer<Word> wordFunction) {
        project.getSections().forEach(section ->
                section.getSentences().forEach(sentence ->
                        sentence.getWords().forEach(wordFunction)));
        return project.getSections();
    }

    public int getWordsNumber(List<Section> sections) {
        return sections.stream()
                .flatMap((section -> section.getSentences().stream()))
                .mapToInt(it -> it.getWords().size())
                .sum();
    }

    public void updateProject(Project project) {
        Optional<Project> updatedProject = projectDao.getProjectByName(project.getName());
        if (updatedProject.isPresent()) {
            project.setId(updatedProject.get().getId());
            projectDao.updateProject(project);
            logStatusChange(project, updatedProject.get().getStatus());
        }
    }

    public Optional<Project> insertProject(String projectName) {
        Optional<Project> newProject = Optional.empty();
        if (projectDao.getProjectByName(projectName).isEmpty()) {
            newProject = Optional.of(projectDao.insertProject(new Project(projectName)));
            log.info("Project: {}, - initialized", projectName);
            log.info("Project: {}, - new status: {}", projectName, Project.Status.CREATED);
        }
        return newProject;
    }

    public List<String> getAllProjectNames() {
        return projectDao.getAllProjects().stream()
                .map(Project::getName)
                .collect(Collectors.toList());
    }

    @Deprecated(forRemoval = true)
    public void deleteAllProjects() {
        log.warn("DELETING ALL PROJECTS");
        projectDao.deleteAllProjects();
    }

    public Optional<Project> getProjectByName(String name) {
        return projectDao.getProjectByName(name);
    }

    private void logStatusChange(Project project, Project.Status previousStatus) {
        if (!previousStatus.equals(project.getStatus())) {
            log.info("Project: {}, - new status: {}", project.getName(), project.getStatus());
        }
    }

    public Optional<Word> getWord(String projectName, String entity) {
        Optional<Project> optionalProject = getProjectByName(projectName);
        AtomicReference<Word> foundWord = new AtomicReference<>(null);
        optionalProject.ifPresent(project ->
                project.getSections().forEach(section ->
                        section.getSentences().forEach(sentence ->
                                sentence.getWords().forEach(word -> {
                                    if (entity.equalsIgnoreCase(word.getBaseForm())) {
                                        foundWord.set(word);
                                    }
                                }))));
        return foundWord.get() == null ? Optional.empty() : Optional.of(foundWord.get());
    }
}
