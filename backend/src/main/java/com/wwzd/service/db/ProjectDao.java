package com.wwzd.service.db;

import com.wwzd.model.db.Project;
import com.wwzd.repository.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ProjectDao {

    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectDao(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project insertProject(Project project) {
        return projectRepository.save(project);
    }

    public Optional<Project> getProjectByName(String name) {
        return projectRepository.findByName(name);
    }

    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    public void updateProject(Project project) {
        projectRepository.save(project);
    }

    public void deleteAllProjects() {
        projectRepository.deleteAll();
    }

//    public Optional<Project> updateProjectStatus(String projectName, Project.Status status) {
//        Optional<Project> project = getProjectByName(projectName);
//        if (project.isPresent()) {
//            //todo check if status changes / write tests
//            project.get().setStatus(status);
//            return updateProject(project.get());
//        }
//        return Optional.empty();
//    }
}
