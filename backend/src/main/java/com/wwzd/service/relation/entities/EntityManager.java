package com.wwzd.service.relation.entities;

import com.wwzd.model.db.Project;
import com.wwzd.service.ProjectService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Log4j2
@Service
public class EntityManager {

    public static final String CUSTOM_ENTITY_CATEGORY = "nam_entity";
    public static final String CUSTOM_NOT_ENTITY_CATEGORY = null;

    private final ProjectService projectService;
    private final EntityCategoryOverrider categoryOverrider;
    private final EntitiesUpdater entitiesUpdater;
    private final EntitiesMerger entitiesMerger;

    @Autowired
    public EntityManager(ProjectService projectService, EntityCategoryOverrider categoryOverrider,
                         EntitiesUpdater entitiesUpdater, EntitiesMerger entitiesMerger) {
        this.projectService = projectService;
        this.categoryOverrider = categoryOverrider;
        this.entitiesUpdater = entitiesUpdater;
        this.entitiesMerger = entitiesMerger;
    }

    public Optional<Project> add(String projectName, String entityName) {
        return insertUpdatedCategories(projectName, entityName, CUSTOM_ENTITY_CATEGORY);
    }

    public Optional<Project> delete(String projectName, String entityName) {
        return insertUpdatedCategories(projectName, entityName, CUSTOM_NOT_ENTITY_CATEGORY);
    }

    public Optional<Project> merge(String projectName, String entityName1, String entityName2) {
        return entitiesMerger.merge(projectName, Pair.of(entityName1, entityName2));
    }

    public Optional<Project> split(String projectName, String entityName1, String entityName2) {
        return entitiesMerger.split(projectName, Pair.of(entityName1, entityName2));
    }

    private Optional<Project> insertUpdatedCategories(String projectName, String entityName, String customNotEntityCategory) {
        Optional<Project> optionalProject = projectService.getProjectByName(projectName);
        optionalProject.ifPresent(project -> {
            project = categoryOverrider.override(project, entityName, customNotEntityCategory);
            entitiesUpdater.updateProjectWithEntitiesAndStatus(project, project.getSections(), project.getGraphInfo());
        });
        return optionalProject;
    }



}
