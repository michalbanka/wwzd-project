package com.wwzd.service.relation.calculation;

import com.wwzd.model.db.Relation;
import com.wwzd.model.db.Section;
import com.wwzd.model.db.Word;

import java.util.List;


public interface RelationStrengthResolver {

    default Relation calculate(List<Section> sections, Word word1, Word word2) {
        int multiplier = 3;
        int wordsLength = word1.getBaseForm().length() + word2.getBaseForm().length();
        int strength = Math.min(wordsLength * multiplier, 100);

        return new Relation(word1, word2, strength);
    }
}
