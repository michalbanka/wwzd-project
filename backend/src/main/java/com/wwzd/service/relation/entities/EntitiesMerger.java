package com.wwzd.service.relation.entities;

import com.wwzd.model.db.GraphInfo;
import com.wwzd.model.db.Project;
import com.wwzd.model.db.Section;
import com.wwzd.service.ProjectService;
import com.wwzd.service.relation.GraphInfoGenerator;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class EntitiesMerger {

    private final ProjectService projectService;
    private final EntityCategoryOverrider categoryOverrider;
    private final AliasManager aliasManager;
    private final GraphInfoGenerator graphInfoGenerator;
    private final EntitiesUpdater entitiesUpdater;

    @Autowired
    public EntitiesMerger(ProjectService projectService, GraphInfoGenerator graphInfoGenerator,
                          AliasManager aliasManager, EntityCategoryOverrider categoryOverrider,
                          EntitiesUpdater entitiesUpdater) {
        this.projectService = projectService;
        this.graphInfoGenerator = graphInfoGenerator;
        this.aliasManager = aliasManager;
        this.categoryOverrider = categoryOverrider;
        this.entitiesUpdater = entitiesUpdater;
    }

    public Optional<Project> merge(String projectName, Pair<String, String> entities) {
        Optional<Project> optionalProject = projectService.getProjectByName(projectName);
        optionalProject.ifPresent(project -> mergeEntities(entities, project));
        return optionalProject;
    }

    public Optional<Project> split(String projectName, Pair<String, String> entities) {
        Optional<Project> optionalProject = projectService.getProjectByName(projectName);
        optionalProject.ifPresent(project -> splitEntities(entities, project));
        return optionalProject;
    }

    private void mergeEntities(Pair<String, String> entities, Project project) {
        setCategory(entities.getSecond(), project, EntityManager.CUSTOM_NOT_ENTITY_CATEGORY);
        List<String> entitiesList = List.of(entities.getFirst(), entities.getSecond());
        List<Section> sections = aliasManager.set(project, entities, entitiesList);

        int relationsPercentage = project.getGraphInfo().getLimit();
        GraphInfo graphInfo = graphInfoGenerator.generate(project.getSections(), relationsPercentage);
        entitiesUpdater.updateProjectWithEntitiesAndStatus(project, sections, graphInfo);
    }

    private void splitEntities(Pair<String, String> entities, Project project) {
        setCategory(entities.getSecond(), project, EntityManager.CUSTOM_ENTITY_CATEGORY);
        List<Section> sections = aliasManager.set(project, entities.getFirst(), getAliases(project.getName(), entities));
        project.setSections(sections);
        sections = aliasManager.set(project, entities.getSecond(), List.of(entities.getSecond()));

        int relationsPercentage = project.getGraphInfo().getLimit();
        GraphInfo graphInfo = graphInfoGenerator.generate(project.getSections(), relationsPercentage);
        entitiesUpdater.updateProjectWithEntitiesAndStatus(project, sections, graphInfo);
    }

    private List<String> getAliases(String projectName, Pair<String, String> entities) {
        List<String> aliases = aliasManager.getAliases(projectName, entities.getFirst());
        aliases.remove(entities.getSecond());
        return aliases;
    }

    private void setCategory(String entity, Project project, String category) {
        categoryOverrider.override(project, entity, category);
    }
}
