package com.wwzd.service.relation.entities;

import com.wwzd.model.db.GraphInfo;
import com.wwzd.model.db.Project;
import com.wwzd.model.db.Section;
import com.wwzd.service.ProjectService;
import com.wwzd.service.relation.GraphInfoGenerator;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
public class EntityCategoryOverrider {

    private final ProjectService projectService;
    private final GraphInfoGenerator graphInfoGenerator;

    @Autowired
    public EntityCategoryOverrider(ProjectService projectService, GraphInfoGenerator graphInfoGenerator) {
        this.projectService = projectService;
        this.graphInfoGenerator = graphInfoGenerator;
    }

    public Project override(Project project, String entityName, String category) {
        updateProjectStatusCalculating(project);
        List<Section> sections = getUpdatedWords(entityName, project, category);
        int relationsPercentage = project.getGraphInfo().getLimit();
        GraphInfo graphInfo = graphInfoGenerator.generate(sections, relationsPercentage);
        return new Project.UpdateBuilder(project)
                .sections(sections)
                .graphInfo(graphInfo)
                .build();
    }

    private void updateProjectStatusCalculating(Project project) {
        projectService.updateProject(new Project.UpdateBuilder(project)
                .status(Project.Status.CALCULATING_RELATIONS)
                .build());
    }

    private List<Section> getUpdatedWords(String entityName, Project project, String category) {
        return projectService.executePerEachWord(project, word -> {
            if (word.getBaseForm().equalsIgnoreCase(entityName)) {
                word.setCategory(category);
            }
        });
    }


}
