package com.wwzd.service.relation;

import com.wwzd.model.db.GraphInfo;
import com.wwzd.model.db.Relation;
import com.wwzd.model.db.Section;
import com.wwzd.model.db.Sentence;
import com.wwzd.model.db.Word;
import com.wwzd.service.relation.calculation.RelationStrengthResolver;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@Service
public class GraphInfoGenerator {

    private final RelationStrengthResolver relationStrengthResolver;

    @Autowired
    public GraphInfoGenerator(RelationStrengthResolver relationStrengthResolver) {
        this.relationStrengthResolver = relationStrengthResolver;
    }

    public GraphInfo generate(List<Section> sections, int relationsPercentage) {
        Set<Word> entities = getEntities(sections);
        Set<Relation> relations = getRelations(sections, entities);
        relations = limitRelations(relations, relationsPercentage);

        return new GraphInfo(relationsPercentage, relations, entities);
    }

    private Set<Word> getEntities(List<Section> sections) {
        return sections.stream()
                .map(Section::getSentences)
                .flatMap(Collection::stream)
                .map(Sentence::getWords)
                .flatMap(Collection::stream)
                .filter(word -> word.getCategory() != null)
                .collect(Collectors.toSet());
    }

    private Set<Relation> getRelations(List<Section> sections, Set<Word> entities) {
        Set<Relation> relations = new HashSet<>();
        entities.forEach(entity1 ->
                entities.forEach(entity2 -> {
                    if (!areEntitiesEqual(entity1, entity2) && !isRelationCalculated(relations, entity1, entity2)) {
                        relations.add(relationStrengthResolver.calculate(sections, entity1, entity2));
                    }
                })
        );
        return relations;
    }

    private boolean areEntitiesEqual(Word entity1, Word entity2) {
        return entity1.equals(entity2);
    }

    private boolean isRelationCalculated(Set<Relation> relations, Word entity1, Word entity2) {
        return relations.contains(new Relation(entity2, entity1, 0));
    }

    private Set<Relation> limitRelations(Set<Relation> relations, int relationsPercentage) {
        int limit = (int)(relationsPercentage / 100.0 * relations.size());
        return relations.stream()
                .sorted(Comparator.comparingInt(Relation::getStrength))
                .limit(limit)
                .collect(Collectors.toSet());
    }

}
