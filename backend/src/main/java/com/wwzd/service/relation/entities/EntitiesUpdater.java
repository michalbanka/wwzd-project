package com.wwzd.service.relation.entities;

import com.wwzd.model.db.GraphInfo;
import com.wwzd.model.db.Project;
import com.wwzd.model.db.Section;
import com.wwzd.service.ProjectService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
public class EntitiesUpdater {

    private final ProjectService projectService;

    public EntitiesUpdater(ProjectService projectService) {
        this.projectService = projectService;
    }

    void updateProjectWithEntitiesAndStatus(Project project, List<Section> sections, GraphInfo graphInfo) {
        projectService.updateProject(new Project.UpdateBuilder(project)
                .sections(sections)
                .graphInfo(graphInfo)
                .status(Project.Status.READY)
                .build());
    }
}
