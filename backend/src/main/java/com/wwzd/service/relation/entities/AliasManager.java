package com.wwzd.service.relation.entities;

import com.wwzd.model.db.Project;
import com.wwzd.model.db.Section;
import com.wwzd.model.db.Word;
import com.wwzd.service.ProjectService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class AliasManager {

    private final ProjectService projectService;

    @Autowired
    public AliasManager(ProjectService projectService) {
        this.projectService = projectService;
    }

    public List<Section> set(Project project, String entity, List<String> aliases) {
        return projectService.executePerEachWord(project, word -> {
            if (entity.equalsIgnoreCase(word.getBaseForm())) {
                word.setAliases(aliases);
            }
        });
    }

    public List<Section> set(Project project, Pair<String, String> entities, List<String> aliases) {
        return projectService.executePerEachWord(project, word -> {
            if (doesWordEqualFirstOrSecond(entities, word)) {
                word.setAliases(aliases);
            }
        });
    }

    private boolean doesWordEqualFirstOrSecond(Pair<String, String> entities, Word word) {
        return word.getBaseForm().equalsIgnoreCase(entities.getFirst())
                || word.getBaseForm().equalsIgnoreCase(entities.getSecond());
    }

    public List<String> getAliases(String projectName, String entity1) {
        Optional<Word> word = projectService.getWord(projectName, entity1);
        List<String> aliases = List.of(entity1);
        if (word.isPresent()) {
            aliases = word.get().getAliases();
        }
        return aliases;

    }
}
