package com.wwzd.service.relation.calculation;

import com.wwzd.model.db.GraphInfo;
import com.wwzd.model.db.Project;
import com.wwzd.service.ProjectService;
import com.wwzd.service.relation.GraphInfoGenerator;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class RelationsCalculator {

    private final GraphInfoGenerator graphInfoGenerator;
    private final ProjectService projectService;

    @Autowired
    public RelationsCalculator(GraphInfoGenerator graphInfoGenerator,
                               ProjectService projectService) {
        this.graphInfoGenerator = graphInfoGenerator;
        this.projectService = projectService;
    }

    public void calculate(String projectName) {
        projectService.getProjectByName(projectName).ifPresent(project -> {
            updateStatus(project, Project.Status.CALCULATING_RELATIONS);
            log.info("Project: {}, - calculating entities relations", projectName);
            int relationsPercentage = project.getGraphInfo().getLimit();
            GraphInfo graph = graphInfoGenerator.generate(project.getSections(), relationsPercentage);
            updateProjectGraphInfo(project, graph);
            updateStatus(project, Project.Status.READY);
        });
    }

    private void updateProjectGraphInfo(Project project, GraphInfo graph) {
        Project updatedProject = new Project.UpdateBuilder(project)
                .graphInfo(graph)
                .build();
        projectService.updateProject(updatedProject);
    }

    private void updateStatus(Project project, Project.Status status) {
        Project updatedProject = new Project.UpdateBuilder(project)
                .status(status)
                .build();
        projectService.updateProject(updatedProject);
    }
}
