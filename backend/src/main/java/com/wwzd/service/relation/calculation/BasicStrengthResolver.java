package com.wwzd.service.relation.calculation;

import com.wwzd.model.db.Relation;
import com.wwzd.model.db.Section;
import com.wwzd.model.db.Word;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;

@Log4j2
@Service
public class BasicStrengthResolver implements RelationStrengthResolver {

    public static final int FAKE_MULTIPLIER = 4;
    public static final int MAX_STRENGTH = 100;

    @Override
    public Relation calculate(List<Section> sections, Word word1, Word word2) {
        List<Integer> strengths = getAliasesStrengths(sections, word1, word2);
        Integer strength = strengths.stream()
                .reduce(getStrengthReducer()).orElse(-1);
        return new Relation(word1, word2, strength);
    }

    private List<Integer> getAliasesStrengths(List<Section> sections, Word word1, Word word2) {
        List<Integer> strengths = new ArrayList<>();
        word1.getAliases().forEach(alias1 ->
                word2.getAliases().forEach(alias2 -> {
                            Integer calculatedStrength = calculateStrength(sections, alias1, alias2);
                            strengths.add(calculatedStrength);
                        }
                ));
        strengths.sort(Integer::compareTo);
        return strengths;
    }

    private int calculateStrength(List<Section> sections, String alias1, String alias2) {
        int wordsLength = alias1.length() + alias2.length();
        return Math.min(wordsLength * FAKE_MULTIPLIER, 100);
    }

    private BinaryOperator<Integer> getStrengthReducer() {
        return (sum, nextVal) -> {
            double multiplier = (MAX_STRENGTH - sum) / (double) MAX_STRENGTH;
            return (int)(multiplier * nextVal + sum);
        };
    }
}
