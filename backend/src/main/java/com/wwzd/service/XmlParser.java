package com.wwzd.service;

import com.wwzd.model.db.Section;
import com.wwzd.model.db.Sentence;
import com.wwzd.model.db.Word;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

@Log4j2
@Service
public class XmlParser {

    private XmlParser() {}

    public static final String XML_WORD_DESCRIPTION_REGEX = "<\\/tok>[\\s\\S]*?<tok>|<tok>|<\\/tok>";

    public static Section convertXmlToSectionObject(String xmlSection) {
        List<Word> words = getXmlWordsDescriptions(xmlSection).stream()
                .map(Word::new)
                .collect(Collectors.toList());
        List<Sentence> sentences = convertWordsToSentences(words);
        return new Section(sentences);
    }

    private static List<Sentence> convertWordsToSentences(List<Word> words) {
        return words.stream().collect(
                ArrayList::new,
                getWordsToSentencesConnector(),
                ArrayList::addAll
        );
    }

    private static BiConsumer<ArrayList<Sentence>, Word> getWordsToSentencesConnector() {
        return (sentences, word) -> {
            if (word.getBaseForm().equals(".")) {
                sentences.add(new Sentence());
            } else {
                if (sentences.isEmpty()) {
                    sentences.add(new Sentence());
                }
                sentences.get(sentences.size() - 1).addWord(word);
            }
        };
    }

    private static List<String> getXmlWordsDescriptions(String xmlSection) {
        List<String> words = Arrays.asList(xmlSection.split(XML_WORD_DESCRIPTION_REGEX));
        try {
            int wordsLastIndex = words.size() - 1;
            return words.subList(1, wordsLastIndex);
        } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
            log.error(e);
            return Collections.emptyList();
        }
    }
}
