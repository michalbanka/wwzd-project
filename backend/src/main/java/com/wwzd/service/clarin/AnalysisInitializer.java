package com.wwzd.service.clarin;

import com.wwzd.model.db.Project;
import com.wwzd.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Slf4j
@Service
public class AnalysisInitializer {

    private static final String START_ANALYZE_URL = "http://ws.clarin-pl.eu/nlprest2/base/startTask/";
    private static final Object DEFAULT_CLARIN_USER = "demo";

    private final RestTemplate restTemplate;
    private final ProjectService projectService;
    private final ClarinTextSender textSender;

    @Autowired
    public AnalysisInitializer(RestTemplate restTemplate, ProjectService projectService, ClarinTextSender textSender) {
        this.restTemplate = restTemplate;
        this.projectService = projectService;
        this.textSender = textSender;
    }

    public Optional<String> start(MultipartFile zippedText, String projectName) {
        Optional<String> analysisId = Optional.empty();
        Optional<Project> project = initializeProject(projectName);
        if (project.isPresent()) {
            String fileHandler = textSender.send(zippedText, projectName);
            analysisId = Optional.ofNullable(executeAnalysis(fileHandler).getBody());
            log.info("Project: {}, - started Clarin analysis", projectName);
        }
        return analysisId;
    }

    private Optional<Project> initializeProject(String projectName) {
        return projectService.insertProject(projectName);
    }

    private ResponseEntity<String> executeAnalysis(String fileHandler) {
        HttpEntity<String> request = getRequestForAnalyzing(fileHandler);
        return restTemplate.exchange(START_ANALYZE_URL, HttpMethod.POST, request, String.class);
    }

    private HttpEntity<String> getRequestForAnalyzing(String fileHandler) {
        return new HttpEntity<>(getRequestBodyForAnalyzing(fileHandler), getHeadersForAnalyzing());
    }

    private HttpHeaders getHeadersForAnalyzing() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    private String getRequestBodyForAnalyzing(String fileHandler) {
        JSONObject requestBody = new JSONObject();
        requestBody.put("lpmn", "filezip(" + fileHandler + ")|any2txt|wcrft2|liner2({\"model\":\"n82\"})|dir|makezip");
        requestBody.put("user", DEFAULT_CLARIN_USER);
        requestBody.put("application", "entity-relations");
        return requestBody.toString();
    }
}
