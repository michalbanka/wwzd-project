package com.wwzd.service.clarin;

import com.wwzd.service.relation.calculation.RelationsCalculator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;
import java.util.concurrent.Executors;

@Slf4j
@Service
public class ClarinClient {

    private final AnalysisInitializer initializer;
    private final AnalysisStatusChecker statusChecker;
    private final AnalysisResultFetcher resultFetcher;
    private final RelationsCalculator relationsCalculator;

    @Autowired
    public ClarinClient(AnalysisInitializer initializer,
                        AnalysisStatusChecker statusChecker,
                        AnalysisResultFetcher resultFetcher,
                        RelationsCalculator relationsCalculator) {
        this.initializer = initializer;
        this.statusChecker = statusChecker;
        this.resultFetcher = resultFetcher;
        this.relationsCalculator = relationsCalculator;
    }

    public Optional<String> analyze(MultipartFile zippedText, String projectName) {
        Optional<String> analysisId = initializer.start(zippedText, projectName);
        analysisId.ifPresent(id ->
                Executors.newSingleThreadExecutor().submit(() -> {
                    Optional<String> resultHandler = statusChecker.check(id, projectName);
                    resultHandler.ifPresent(handler -> {
                        resultFetcher.fetchResult(handler, projectName);
                        relationsCalculator.calculate(projectName);
                    });
                })
        );
        return analysisId;
    }

}
