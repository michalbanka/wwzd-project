package com.wwzd.service.clarin;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wwzd.model.db.Project;
import com.wwzd.service.ProjectService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.InvalidParameterException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
public class AnalysisStatusChecker {

    private static final String CLARIN_CHECK_STATUS_URL = "http://ws.clarin-pl.eu/nlprest2/base/getStatus/";

    private final ObjectMapper objectMapper;
    private final RestTemplate restTemplate;
    private final ProjectService projectService;

    @Autowired
    public AnalysisStatusChecker(ObjectMapper objectMapper, RestTemplate restTemplate, ProjectService projectService) {
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
        this.projectService = projectService;
    }

    public Optional<String> check(String processId, String projectName) {
        Optional<String> fileHandler = Optional.empty();
        updateStatusToClarinAnalysis(projectName);
        try {
            do {
                TimeUnit.SECONDS.sleep(5);
                fileHandler = getFileHandler(processId);
            } while (fileHandler.isEmpty());
        } catch (JsonProcessingException | InterruptedException e) {
            log.error("Couldn't get Clarin analysis", e);
            Thread.currentThread().interrupt();
        }
        return Optional.of(fileHandler.orElseThrow(InvalidParameterException::new).split("\"")[1].split("/")[3]);
    }

    private void updateStatusToClarinAnalysis(String projectName) {
        Optional<Project> project = projectService.getProjectByName(projectName);
        if (project.isPresent()) {
            Project updatedProject = new Project.UpdateBuilder(project.get())
                    .status(Project.Status.CLARIN_ANALYSIS)
                    .build();
            projectService.updateProject(updatedProject);
        }
    }

    private Optional<String> getFileHandler(String processId) throws JsonProcessingException {
        String response = getClarinStatusResponse(processId);
        return getFileHandlerFromClarinResponse(response);
    }

    private String getClarinStatusResponse(String processId) {
        return restTemplate.exchange(
                CLARIN_CHECK_STATUS_URL + processId,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class).getBody();
    }

    private Optional<String> getFileHandlerFromClarinResponse(String clarinResponse) throws JsonProcessingException {
        Optional<String> status;
        try {
            status = Optional.of(objectMapper
                    .readValue(clarinResponse, JsonNode.class)
                    .get("value")
                    .get(0)
                    .get("fileID")
                    .toString());
        } catch (NullPointerException e) {
            status = Optional.empty();
        }
        return status;
    }
}
