package com.wwzd.service.clarin;

import com.wwzd.model.db.Project;
import com.wwzd.service.ProjectService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Log4j2
@Service
public class ClarinTextSender {

    private static final String CLARIN_UPLOAD_URL = "http://ws.clarin-pl.eu/nlprest2/base/upload/";

    private final RestTemplate restTemplate;
    private final ProjectService projectService;

    @Autowired
    public ClarinTextSender(RestTemplate restTemplate, ProjectService projectService) {
        this.restTemplate = restTemplate;
        this.projectService = projectService;
    }

    public String send(MultipartFile zippedText, String projectName) {
        log.info("Project: {}, - sending text to Clarin", projectName);
        updateStatusToSendingZip(projectName);
        return sendFile(zippedText).getBody();
    }

    private void updateStatusToSendingZip(String projectName) {
        Optional<Project> project = projectService.getProjectByName(projectName);
        if (project.isPresent()) {
            Project updatedProject = new Project.UpdateBuilder(project.get())
                    .status(Project.Status.SENDING_TEXT)
                    .build();
            projectService.updateProject(updatedProject);
        }
    }

    private ResponseEntity<String> sendFile(MultipartFile zipBytes) {
        HttpEntity<MultiValueMap<String, Object>> request = getRequest(zipBytes);
        return restTemplate.exchange(CLARIN_UPLOAD_URL, HttpMethod.POST, request, String.class);
    }

    private HttpEntity<MultiValueMap<String, Object>> getRequest(MultipartFile zipBytes) {
        return new HttpEntity<>(getRequestBodyForAnalyzing(zipBytes), getHeadersForAnalyzing());
    }

    private HttpHeaders getHeadersForAnalyzing() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return headers;
    }

    private MultiValueMap<String, Object> getRequestBodyForAnalyzing(MultipartFile zipBytes) {
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", zipBytes.getResource());
        return body;
    }
}
