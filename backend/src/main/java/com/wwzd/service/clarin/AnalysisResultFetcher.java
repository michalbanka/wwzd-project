package com.wwzd.service.clarin;

import com.wwzd.model.db.Project;
import com.wwzd.model.db.Section;
import com.wwzd.service.FilesManager;
import com.wwzd.service.ProjectService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class AnalysisResultFetcher {

    private static final String CLARIN_DOWNLOAD_RESULT_ZIP_URL = "http://ws.clarin-pl.eu/nlprest2/base/download/requests/makezip/";

    private final ProjectService projectService;
    private final FilesManager filesManager;
    private final RestTemplate restTemplate;

    @Autowired
    public AnalysisResultFetcher(ProjectService projectService,
                                 FilesManager filesManager,
                                 RestTemplate restTemplate) {
        this.projectService = projectService;
        this.filesManager = filesManager;
        this.restTemplate = restTemplate;
        this.restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
    }

    public synchronized void fetchResult(String fileHandler, String projectName) {
        try {
            getClarinResult(fileHandler, projectName);
        } catch (IOException e) {
            log.error("Couldn't fetch result from Clarin.", e);
        }
    }

    public void getClarinResult(String fileHandler, String projectName) throws IOException {
        Optional<Project> project = projectService.getProjectByName(projectName);
        if (project.isPresent()) {
            String zipPath = downloadResultZip(fileHandler, projectName);
            log.info("Project: {}, - extracting result ZIP", projectName);
            List<Section> sections = filesManager.getDataFromZip(zipPath);
            updateProjectSections(projectName, sections);
        }
    }

    private String downloadResultZip(String fileHandler, String projectName) throws IOException {
        HttpEntity<String> request = new HttpEntity<>(getHeadersForFetching());
        log.info("Project: {}, - downloading result ZIP from Clarin", projectName);
        updateStatusToFetchResultFromClarin(projectName);
        ResponseEntity<byte[]> response = executeFetching(fileHandler, request);
        String zipFullPath = "";
        if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
            zipFullPath = filesManager.saveZip(fileHandler, response.getBody());
        }
        return zipFullPath;
    }

    private void updateStatusToFetchResultFromClarin(String projectName) {
        Optional<Project> project = projectService.getProjectByName(projectName);
        if (project.isPresent()) {
            Project updatedProject = new Project.UpdateBuilder(project.get())
                    .status(Project.Status.FETCHING_RESULT_FROM_CLARIN)
                    .build();
            projectService.updateProject(updatedProject);
        }
    }

    private HttpHeaders getHeadersForFetching() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
        return headers;
    }

    private ResponseEntity<byte[]> executeFetching(String fileHandler, HttpEntity<String> request) {
        return restTemplate.exchange(
                CLARIN_DOWNLOAD_RESULT_ZIP_URL + fileHandler,
                HttpMethod.GET,
                request,
                byte[].class);
    }

    private void updateProjectSections(String projectName, List<Section> sections) {
        Optional<Project> project = projectService.getProjectByName(projectName);
        if (project.isPresent()) {
            Project updatedProject = new Project.UpdateBuilder(project.get())
                    .wordsNumber(projectService.getWordsNumber(sections))
                    .sections(sections)
                    .build();
            projectService.updateProject(updatedProject);
        }
    }
}
