package com.wwzd.controller;

import com.wwzd.service.clarin.ClarinClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Log4j2
@RestController
@RequestMapping("/")
public class ClarinAnalysisController {

    private final ClarinClient clarinClient;

    @Autowired
    public ClarinAnalysisController(ClarinClient clarinClient) {
        this.clarinClient = clarinClient;
    }

    @PostMapping("/analyze")
    public ResponseEntity<Object> getAnalysis(@RequestParam String projectName,
                                              @RequestParam("file") MultipartFile zippedText) {
        Optional<String> analysisId = clarinClient.analyze(zippedText, projectName);

        ResponseEntity<Object> response = ResponseEntity.ok().build();
        if (analysisId.isEmpty()) {
            response = ResponseEntity.badRequest().body("Project with this name exists");
        }
        return response;
    }
}
