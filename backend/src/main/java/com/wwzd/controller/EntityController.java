package com.wwzd.controller;

import com.wwzd.model.db.Project;
import com.wwzd.service.relation.entities.EntityManager;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@Log4j2
@RestController
@RequestMapping("/entity")
public class EntityController {

    private static final String PROJECT_NOT_EXISTS_MESSAGE = "Project with this name does not exist";
    private final EntityManager entityManager;

    @Autowired
    public EntityController(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @PutMapping("/add")
    public ResponseEntity<Object> addEntity(@RequestParam String projectName,
                                            @RequestParam String entityName) {

        Optional<Project> project = entityManager.add(projectName, entityName);

        return project
                .<ResponseEntity<Object>>map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.badRequest().body(PROJECT_NOT_EXISTS_MESSAGE));
    }

    @PutMapping("/merge")
    public ResponseEntity<Object> mergeEntities(@RequestParam String projectName,
                                                @RequestParam String entityNameTo,
                                                @RequestParam String entityNameFrom) {
        Optional<Project> project = entityManager.merge(projectName, entityNameTo, entityNameFrom);

        return project
                .<ResponseEntity<Object>>map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.badRequest().body(PROJECT_NOT_EXISTS_MESSAGE));
    }

    @PutMapping("/split")
    public ResponseEntity<Object> splitEntities(@RequestParam String projectName,
                                                @RequestParam String entityNameFrom,
                                                @RequestParam String entityNameTo) {
        Optional<Project> project = entityManager.split(projectName, entityNameFrom, entityNameTo);

        return project
                .<ResponseEntity<Object>>map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.badRequest().body(PROJECT_NOT_EXISTS_MESSAGE));
    }

    @DeleteMapping
    public ResponseEntity<Object> deleteEntity(@RequestParam String projectName,
                                               @RequestParam String entityName) {

        Optional<Project> project = entityManager.delete(projectName, entityName);

        return project
                .<ResponseEntity<Object>>map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.badRequest().body(PROJECT_NOT_EXISTS_MESSAGE));
    }
}
