package com.wwzd.controller;

import com.wwzd.model.db.GraphInfo;
import com.wwzd.model.db.Project;
import com.wwzd.model.db.Relation;
import com.wwzd.model.db.Word;
import com.wwzd.service.ProjectService;
import com.wwzd.service.relation.GraphInfoGenerator;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.Set;

@Log4j2
@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;
    private final GraphInfoGenerator graphInfoGenerator;

    @Autowired
    public ProjectController(ProjectService projectService, GraphInfoGenerator graphInfoGenerator) {
        this.projectService = projectService;
        this.graphInfoGenerator = graphInfoGenerator;
    }

    @GetMapping("/data")
    public ResponseEntity<Object> getProject(@RequestParam String projectName) {

        Optional<Project> project = projectService.getProjectByName(projectName);

        return project
                .<ResponseEntity<Object>> map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping("/status")
    public ResponseEntity<Object> getStatus(@RequestParam String projectName) {

        Optional<Project> project = projectService.getProjectByName(projectName);

        return project
                .<ResponseEntity<Object>> map(project1 -> ResponseEntity.ok(project1.getStatus()))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PutMapping("/limit")
    public ResponseEntity<Object> getStatus(@RequestParam String projectName,
                                            @RequestParam int limit) {

        Optional<Project> project = projectService.getProjectByName(projectName);
        if (project.isPresent()) {
            GraphInfo graphInfo = graphInfoGenerator.generate(project.get().getSections(), limit);
            projectService.updateProject(new Project.UpdateBuilder(project.get()).graphInfo(graphInfo).build());
        }
        project = projectService.getProjectByName(projectName);

        return project
                .<ResponseEntity<Object>> map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping()
    public ResponseEntity<Object> getAllProjectNames() {
        return ResponseEntity.ok().body(projectService.getAllProjectNames());
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Object> deleteAll() {
        projectService.deleteAllProjects();
        return ResponseEntity.ok().build();
    }


}
