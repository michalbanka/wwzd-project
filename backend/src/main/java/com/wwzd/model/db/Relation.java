package com.wwzd.model.db;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(exclude = "strength")
public class Relation {
    private Word word1;
    private Word word2;
    private int strength;
}
