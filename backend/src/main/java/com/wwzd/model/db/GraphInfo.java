package com.wwzd.model.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class GraphInfo {
    private int limit;
    private Set<Relation> relations;
    private Set<Word> entities;
}
