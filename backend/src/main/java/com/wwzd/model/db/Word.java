package com.wwzd.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "baseForm")
public class Word {
    private static final String BASE_FORM_OPENING_TAG = "<base>";
    private static final String BASE_FORM_CLOSING_TAG = "</base>";
    private static final String CATEGORY_OPENING_TAG = "<ann";
    private static final String CATEGORY_CLOSING_TAG = "</ann>";
    private static final String IS_ENTITY_REGEX = ".*nam_(liv|loc|entity).*>[1-9]?";

    private String baseForm;
    private String category;
    private List<String> aliases;

    public Word(String xmlForm) {
        this.baseForm = parseBaseForm(xmlForm);
        this.category = parseCategory(xmlForm);
        this.aliases = List.of(baseForm);
    }

    @JsonIgnore
    public boolean isEntity() {
        return StringUtils.isNotBlank(baseForm) && category != null && !category.isEmpty();
    }

    @JsonIgnore
    private String parseBaseForm(String xmlForm) {
        return StringUtils.substringBetween(xmlForm, BASE_FORM_OPENING_TAG, BASE_FORM_CLOSING_TAG);
    }

    @JsonIgnore
    private String parseCategory(String xmlForm) {
        String parsedCategory = null;
        String[] xmlCategories = StringUtils.substringsBetween(xmlForm, CATEGORY_OPENING_TAG, CATEGORY_CLOSING_TAG);
        if (xmlCategories != null && xmlCategories.length > 0) {
            List<String> categories = Arrays.stream(xmlCategories)
                    .filter(cat -> cat.matches(IS_ENTITY_REGEX))
                    .map(cat -> StringUtils.substringBetween(cat, "chan=\"", "\""))
                    .collect(Collectors.toList());
            if (!categories.isEmpty()) parsedCategory = categories.get(0);
        }
        return parsedCategory;
    }


}