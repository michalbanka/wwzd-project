package com.wwzd.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Document(collection = "projects")
public class Project {
    @Id
    private String id;
    private Status status;
    private String name;
    private Integer wordsNumber;
    private GraphInfo graphInfo;
    @JsonIgnore
    private List<Section> sections;

    public Project(String name) {
        this.id = null;
        this.status = Status.CREATED;
        this.name = name;
        this.wordsNumber = 0;
        this.graphInfo = new GraphInfo(10, Collections.emptySet(), Collections.emptySet());
        this.sections = Collections.emptyList();
    }

    public enum Status {
        CREATED,
        SENDING_TEXT,
        CLARIN_ANALYSIS,
        FETCHING_RESULT_FROM_CLARIN,
        CALCULATING_RELATIONS,
        READY
    }

    public static final class UpdateBuilder {
        private Project project;

        public UpdateBuilder(Project project) {
            this.project = project;
        }

        public UpdateBuilder id(String id) {
            project.setId(id);
            return this;
        }

        public UpdateBuilder status(Project.Status status) {
            project.setStatus(status);
            return this;
        }

        public UpdateBuilder name(String name) {
            project.setName(name);
            return this;
        }

        public UpdateBuilder wordsNumber(Integer wordsNumber) {
            project.setWordsNumber(wordsNumber);
            return this;
        }

        public UpdateBuilder sections(List<Section> sections) {
            project.setSections(sections);
            return this;
        }

        public UpdateBuilder graphInfo(GraphInfo graphInfo) {
            project.setGraphInfo(graphInfo);
            return this;
        }

        public Project build() {
            return project;
        }
    }
}

