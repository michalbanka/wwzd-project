import http from "../http-common";

const CONTROLLER_NAME = '/entity';

const addEntity = data => {
    console.log(data)
    return http.put(`${CONTROLLER_NAME}/add?projectName=${data.projectName}&entityName=${data.entityName}`);
};

const mergeEntity = data => {
    return http.put(`${CONTROLLER_NAME}/merge?projectName=${data.projectName}&entityNameTo=${data.entityNameTo}&entityNameFrom=${data.entityNameFrom}`);
};

const splitEntity = data => {
    return http.put(`${CONTROLLER_NAME}/split?projectName=${data.projectName}&entityNameTo=${data.entityNameTo}&entityNameFrom=${data.entityNameFrom}`);
}

const deleteEntity = data => {
    return http.delete(`${CONTROLLER_NAME}?projectName=${data.projectName}&entityName=${data.entityName}`)
}

export default {
    addEntity,
    mergeEntity,
    splitEntity,
    deleteEntity
}