import React, { useState } from "react";
import styled from "styled-components";
import EntitiesService from "./EntitiesService"

const DeleteEntity = (projectNameId) => {

  const entityDeleteModelState = {
    projectName: projectNameId,
    entityName: "",
  };

  const [entityDelete, setEntityDelete] = useState(entityDeleteModelState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = event => {
    const {name,value} = event.target;
    setEntityDelete({...entityDelete, [name]: value})
  }

  const saveEntityAdd = () => {
    var data = {
      projectName: entityDelete.projectName.projectNameId.projectName,
      entityName: entityDelete.entityName
    }
    EntitiesService.deleteEntity(data)
      .then(response => {
        alert(response);
        setSubmitted(true);
      }) 
      .catch(e => {
        alert(e);
        console.log(e);
        console.log(data);
      });
  }

  const newDeleteEntity = () => {
    setEntityDelete(entityDeleteModelState);
    setSubmitted(false);
  }

    return (
      <div>
        <h2>Delete entity</h2>
        <label htmlFor="entityName">Delete entity: </label>
        <input type="text" 
        id="entityName"
        required
        value={entityDelete.entityName}
        onChange={handleInputChange}
        name="entityName"
        />
        <button onClick={saveEntityAdd}> Delete </button>
      </div>
    );
}

const FormWrapper = styled.div`
`;
const UploadWrapper = styled.div`
  margin-left: calc(255px + 30px);
  margin-top: 30px;
  margin-right: 30px;
  background: #ffffff;
  box-shadow: 5px 10px 8px #888888;
`;

const NavUpload = styled.div`
  text-align: center;
  font-size: 30px;
  padding: 10px;
  background: #99cb50;
`;

const Name = styled.div`
  height: 40px;
  font-size: 30px;
  background: #99cb50;
  opacity: 0.5;
  font-size: 22px;
`;

const StyledInputName = styled.input`
  height: 40px;
  font-size: 30px;
  background: #99cb50;
  opacity: 0.5;
  font-size: 22px;
  width: calc(100% - 5px);
  border: none;
`;

const Details = styled.div`
  margin-right: 30px;
  margin-left: 30px;
  margin-bottom: 30px;
`;

const Buttons = styled.div`
  margin: 30px;
  padding-bottom: 30px;
`;

const StyledInput = styled.input`
  opacity: 0;
  width: 0.1px;
  height: 0.1px;
  position: absolute;
`;

const StyledLabel = styled.label`
  box-shadow: inset 0px 1px 0px 0px #a4e271;
  background: linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color: #89c403;
  border-radius: 6px;
  border: 1px solid #74b807;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;
  font-family: Arial;
  font-size: 15px;
  font-weight: bold;
  padding: 6px 24px;
  text-decoration: none;
  text-shadow: 0px 1px 0px #528009;
  margin-right: 20px;
`;

const StyledButton = styled.button`
  box-shadow: inset 0px 1px 0px 0px #a4e271;
  background: linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color: #89c403;
  border-radius: 6px;
  border: 1px solid #74b807;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;
  font-family: Arial;
  font-size: 15px;
  font-weight: bold;
  padding: 6px 24px;
  text-decoration: none;
  text-shadow: 0px 1px 0px #528009;
`;

export default DeleteEntity;