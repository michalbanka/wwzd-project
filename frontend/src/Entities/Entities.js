import React from "react";
import styled from "styled-components";
import AddEntities from "./AddEntities";
import DeleteEntity from "./DeleteEntity";
import MergeEntity from "./MergeEntity";
import SplitEntity from "./SplitEntity";

const Entities = (projectName) => {

    return (
        <FormWrapper>
            <Nav>Entities panel</Nav>
            <AddEntities projectNameId={projectName}></AddEntities>
            <MergeEntity projectNameId={projectName}></MergeEntity>
            <SplitEntity projectNameId={projectName}></SplitEntity>
            <DeleteEntity projectNameId={projectName}></DeleteEntity>
        </FormWrapper>
    )
}

const FormWrapper = styled.div`
  margin-left: calc(255px + 30px);
  margin-top: 30px;
  margin-right: 30px;
  background: #ffffff;
  box-shadow: 5px 10px 8px #888888;
`;

const Nav = styled.div`
  text-align: center;
  font-size: 30px;
  padding: 10px;
  background: #99cb50;
`;

export default Entities;