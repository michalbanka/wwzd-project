import Home from './Home/Home.js'
import styled from "styled-components";

import "./App.css";

function App() {
  return (
    <HomeWrapper>
      <Home />
    </HomeWrapper>
  );
}

const HomeWrapper = styled.div`
background: #f3f4fa;
min-height: 100vh;

`;

export default App;
