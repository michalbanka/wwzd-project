import React from "react";
import styled from "styled-components";

function Sidebar(props) {
  return <SidebarWrapper>Sidebar</SidebarWrapper>;
}

const SidebarWrapper = styled.div`
  min-height: calc(100vh - 80px);
  background: #44525f;
  font-family: Roboto, sans-serif;
  padding: 0;
  width: 255px;
  display: flex;
  float: left;
`;
export default Sidebar;
