import React from "react";
import styled from "styled-components";
import PLogo from "./svg/Logo.png";

function Navbar(props) {
  return (
    <NavbarWrapper>
      <LogoWrapper>
        <Logo src={PLogo} />
      </LogoWrapper>
    </NavbarWrapper>
  );
}

const NavbarWrapper = styled.div`
  color: #fff;
  padding-right: 15px;
  height: 80px;
  background: #99cb50;
  display: flex;
`;

const LogoWrapper = styled.div`
  background: #78b720;
  height: 80px;
  width: 255px;
`;
const Logo = styled.img`
  height: 75px;
  width: 75px;
  display: block;
  margin-left: auto;
  margin-right: auto;
`;

export default Navbar;
