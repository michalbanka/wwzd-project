export const pushEntities = (emptyArray, entities) => {
  entities.map((name) =>
    emptyArray.push({
      id: String(name.baseForm),
      data: {
        label: String(name.baseForm),
      },
      position: {
        x: Math.floor(Math.random() * 1000) + 1,
        y: Math.floor(Math.random() * 1000) + 1,
      },
    })
  );
};

export const pushRelations = (notEmptyArray, relations) => {
  relations.map((name) =>
    notEmptyArray.push({
      id: "e" + String(name.word1.baseForm) + "-" + String(name.word2.baseForm),
      source: String(name.word1.baseForm),
      target: String(name.word2.baseForm),
    })
  );
};
