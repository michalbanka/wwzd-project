import React, { useState, useCallback } from "react";
import ReactFlow, {
  removeElements,
  addEdge,
  MiniMap,
  Controls,
  Background,
} from "react-flow-renderer";
import initialElements from "./initial-elements";
import { pushEntities, pushRelations } from "./utils";

const onLoad = (reactFlowInstance) => {
  console.log("flow loaded:", reactFlowInstance);
  reactFlowInstance.fitView();
};

const OverviewFlow = ({ dataGraf }) => {
  let entities = "";
  let nodeName = [];
  let relations = "";
  if (dataGraf != null) {
    // eslint-disable-next-line no-const-assign
    entities = dataGraf.graphInfo.entities;
    relations = dataGraf.graphInfo.relations;
    pushEntities(nodeName, entities);
    pushRelations(nodeName, relations);
  }

  return (
    <>
      {nodeName.length > 0 ? (
        <ReactFlow
          elements={nodeName}
          onLoad={onLoad}
          snapToGrid={true}
          snapGrid={[15, 15]}
          style={{
            width: "86%",
            height: "90vh",
            position: "absolute",
            margin: "0 0 0 255px",
          }}
        >
          <MiniMap
            nodeStrokeColor={(n) => {
              if (n.style?.background) return n.style.background;
              if (n.type === "input") return "#0041d0";
              if (n.type === "output") return "#ff0072";
              if (n.type === "default") return "#1a192b";
              return "#eee";
            }}
            nodeColor={(n) => {
              if (n.style?.background) return n.style.background;
              return "#fff";
            }}
            nodeBorderRadius={2}
          />
          <Controls />
          <Background color="#aaa" gap={16} />
        </ReactFlow>
      ) : null}
    </>
  );
};
export default OverviewFlow;
