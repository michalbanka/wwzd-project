import axios from "axios";
import styled from "styled-components";
import React, { useState, useEffect } from "react";
import PacmanLoader from "react-spinners/PacmanLoader";
import OkLogo from "../Nav/svg/ok.png";
import { withRouter } from "react-router-dom";
import Entities from '../Entities/Entities';

const Status = withRouter(
  ({ checkStatus, projectName, setDataGraf, history }) => {
    console.log("SPRAWDZ", checkStatus);
    const [status, setStatus] = useState("BRAK");
    const [shouldRender, setShouldRender] = useState(false);

    const color = "#99cb50";

    const sendGetRequest = async () => {
      try {
        const resp = await axios.get("http://localhost:8080/project/status", {
          params: { projectName: projectName },
        });
        setStatus(resp.data);
      } catch (err) {
        // Handle Error Here
        console.error(err);
      }
    };

    const stillSendRequest = () => {
      setShouldRender(true);
      let i = 0;
      let interval = setInterval(() => {
        if (i < 16) {
          sendGetRequest();
          i++;
          console.log("Waiting for the next call.");
        } else {
          clearInterval(interval);
        }
      }, 5000);
    };

    useEffect(() => {
      if (checkStatus) {
        stillSendRequest();
      }
    });

    const sendGetToGraf = () => {
      axios
        .get("http://localhost:8080/project/data", {
          params: { projectName: projectName },
        })
        .then(
          (response) => {
            setDataGraf(response.data);
            console.log(response.data.graphInfo.entities);
            console.log(response);
          },
          (error) => {
            console.log(error);
          }
        );
    };

    return (
      <>
        {shouldRender ? (
          <StatusWrapper>
            {status !== "READY" ? (
              <PacmanLoader color={color} size={150} />
            ) : (
              <>
                <Entities projectName={projectName}/>
                <img src={OkLogo} alt="description" />
                <StyledButton
                  onClick={() => {
                    sendGetToGraf();
                    history.push("/graf");
                  }}
                >
                  Przejdz do grafu
                </StyledButton>
                
              </>
            )}
          </StatusWrapper>
        ) : null}
        

      </>
    );
  }
);

const StatusWrapper = styled.div`
  margin-left: calc(255px + 130px);
  margin-top: 130px;
  margin-right: 30px;
  width: 40%;
`;

const EntitiesWrapper = styled.div``;

const FullWrapper = styled.div``;

const StyledButton = styled.button`
  box-shadow: inset 0px 1px 0px 0px #a4e271;
  background: linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color: #89c403;
  border-radius: 6px;
  border: 1px solid #74b807;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;
  font-family: Arial;
  font-size: 15px;
  font-weight: bold;
  padding: 6px 24px;
  text-decoration: none;
  text-shadow: 0px 1px 0px #528009;
`;

export default Status;
