import axios from "axios";
import styled from "styled-components";
import React, { useState } from "react";

const Upload = ({ changeStatus, projectName, setProjectName }) => {
  const [selectedFile, setSelectedFile] = useState(null);

  const onFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const onFileUpload = () => {
    const formData = new FormData();
    formData.append("file", selectedFile);
    formData.append("projectName", projectName);
    console.log(selectedFile);

    axios.post("http://localhost:8080/analyze", formData).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
  };

  const handleUploadAndStatus = () => {
    onFileUpload();
    changeStatus(true);
  };

  const handleProjectName = (e) => {
    setProjectName(e.target.value)
  };

  return (
    <UploadWrapper>
      <NavUpload>Wybierz plik do analizy</NavUpload>
      <Details>
        <p>Name:</p>
        <StyledInputName
          type="text"
          name="name"
          placeholder="Name"
          value={projectName}
          onChange={handleProjectName}
        />
        <p>Choose a file: </p>
        {selectedFile ? (
          <Name>{selectedFile.name}</Name>
        ) : (
          <Name>Select ZIP</Name>
        )}
      </Details>
      <Buttons>
        <StyledInput type="file" id="file" onChange={onFileChange} />
        <StyledLabel for="file">Select file</StyledLabel>
        <StyledButton onClick={handleUploadAndStatus} disabled={!projectName}>Upload</StyledButton>
      </Buttons>
    </UploadWrapper>
  );
};

const UploadWrapper = styled.div`
  margin-left: calc(255px + 30px);
  margin-top: 30px;
  margin-right: 30px;
  background: #ffffff;
  box-shadow: 5px 10px 8px #888888;
`;

const NavUpload = styled.div`
  text-align: center;
  font-size: 30px;
  padding: 10px;
  background: #99cb50;
`;

const Name = styled.div`
  height: 40px;
  font-size: 30px;
  background: #99cb50;
  opacity: 0.5;
  font-size: 22px;
`;

const StyledInputName = styled.input`
  height: 40px;
  font-size: 30px;
  background: #99cb50;
  opacity: 0.5;
  font-size: 22px;
  width: calc(100% - 5px);
  border: none;
`;

const Details = styled.div`
  margin-right: 30px;
  margin-left: 30px;
  margin-bottom: 30px;
`;

const Buttons = styled.div`
  margin: 30px;
  padding-bottom: 30px;
`;

const StyledInput = styled.input`
  opacity: 0;
  width: 0.1px;
  height: 0.1px;
  position: absolute;
`;

const StyledLabel = styled.label`
  box-shadow: inset 0px 1px 0px 0px #a4e271;
  background: linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color: #89c403;
  border-radius: 6px;
  border: 1px solid #74b807;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;
  font-family: Arial;
  font-size: 15px;
  font-weight: bold;
  padding: 6px 24px;
  text-decoration: none;
  text-shadow: 0px 1px 0px #528009;
  margin-right: 20px;
`;

const StyledButton = styled.button`
  box-shadow: inset 0px 1px 0px 0px #a4e271;
  background: linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color: #89c403;
  border-radius: 6px;
  border: 1px solid #74b807;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;
  font-family: Arial;
  font-size: 15px;
  font-weight: bold;
  padding: 6px 24px;
  text-decoration: none;
  text-shadow: 0px 1px 0px #528009;
`;

export default Upload;
