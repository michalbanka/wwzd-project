import React, { useState } from "react";
import styled from "styled-components";
import Sidebar from "../Nav/Sidebar.js";
import Navbar from "../Nav/Navbar.js";
import Upload from "../Upload/Upload";
import Status from "../Status/Status";
import OverviewFlow from "../Graph/index";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Entities from "../Entities/Entities.js";

const Home = () => {
  const [checkStatus, setCheckStatus] = useState(false);
  const [projectName, setProjectName] = useState();
  const [dataGraf, setDataGraf] = useState(null);

  console.log("TUTAJ SPRAWDZ", checkStatus);
  return (
    <Router>
      <div>
        <Navbar />
        <Test>
          <Sidebar />
          <Switch>
            <Route path="/home">
              <Upload
                changeStatus={setCheckStatus}
                setProjectName={setProjectName}
                projectName={projectName}
              />

              <Status
                checkStatus={checkStatus}
                projectName={projectName}
                setDataGraf={setDataGraf}
              />
            </Route>
            <Route path="/graf">
              <OverviewFlow dataGraf={dataGraf} />
            </Route>
          </Switch>
        </Test>
      </div>
    </Router>
  );
};

const Test = styled.div`
  overflow: hidden;
  background: #f1f1f1;
`;

export default Home;
