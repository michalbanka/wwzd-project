# Visualization of entities relations in big texts

## How to run from DockerHub images?
Required:
- docker
- docker-compose
 
Execute this (preferred way):
```
make start-public
```
or:
```
docker-compose -f ./docker-compose-external.yml up
```

### How to check whether containers are correctly setup?
Run ``docker ps`` which should return all containers running on system.

## How to run from locally build images?
Required:
- java 11
- maven
- docker
- docker-compose

Just ``docker-compose up`` may be not enough with local builds.

Execute this:
```
make start
```

## How to run backend debugger via IntelliJ?
1. Set up database container
    ```
    docker-compose up -d entities-visualizer-db
    ```
2. Run backend project from IntelliJ e.g. using debugger.

## How to peek into database through MongoDB GUI client - CompassDB?
1. Install CompassDB.
2. In connection configuration paste:
```
mongodb://user1:secretpassword@localhost:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false
```
