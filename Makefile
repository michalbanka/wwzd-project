# Soft clean and start containers on newly built image.
start: build --run-containers-prebuild --soft-clean-containers

# Clean runtime environment EXPECT database container.
soft-clean: --soft-clean-containers --remove-image

# Clean everything from runtime env.
hard-clean: --remove-image
	docker-compose down

build:
	mvn clean package -DskipTests -f ./backend/pom.xml

start-public:
	docker-compose -f ./docker-compose-external.yml up

--run-containers-prebuild:
	docker-compose up --build

--soft-clean-containers:
	docker-compose rm -f entities-visualizer-server

--remove-image:
	docker image rm -f entities-visualizer-server
